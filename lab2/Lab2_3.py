import cherrypy
import json

class Calculator(object):
    exposed=True

    def __init__(self):
        pass

    def PUT(self,**params):
        body=cherrypy.request.body.read()
        json_body=json.loads(body)
	
        values=list(json_body.values())
        operands=values[1]

        for i in range(len(operands)):
            operands[i]=float(operands[i])

        result=operands[0]

        if len(operands)!=0:
            if len(operands)>1:
                if values[0] == 'add':
                    for i in range(1, len(operands)):
                        result += operands[i]
                elif values[0] == 'sub':
                    for i in range(1, len(operands)):
                        result -= operands[i]
                elif values[0] == 'mul':
                    for i in range(1, len(operands)):
                        result *= operands[i]
                elif values[0] == 'div':
                    for i in range(1, len(operands)):
                        if operands[i] != 0:
                            result = result / operands[i]
                        else:
                            result = 'Division error'
                else:
                    result = 'Incorrect command'
            else:
                result = 'Invalid number of operands'
        
            dictionary={
                "operation": values[0],
                "operands": operands,
                "result": result,
            }
        json_object = json.dumps(dictionary, indent=3)
        return json_object  

          

if __name__=="__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.quickstart(Calculator(),'/',conf)
    #cherrypy.config.update({'server.socket_port':8090})