import cherrypy
import json

class Calculator(object):
    exposed=True
    def GET(self,*uri,**params):

        result=0
       
        for key in params:
            params[key]=float(params[key])

        if len(uri)!=0:
            if len(params)==2:
                if uri[0] == 'add':
                    result = params['op1'] + params['op2']
                elif uri[0] == 'sub':
                    result = params['op1'] - params['op2']
                elif uri[0] == 'mul':
                    result = params['op1'] * params['op2']
                elif uri[0] == 'div':
                    if params['op2'] != 0:
                        result = params['op1'] / params['op2']
                    else:
                        result = 'Division error'
                else:
                    result = 'Incorrect command'
            else:
                result = 'Invalid number of operands'
        
            dictionary={
                "operation": uri[0],
                "operands": {"op1": params['op1'], "op2": params['op2']},
                "result": result,
            }
            json_object = json.dumps(dictionary, indent=3)
        return json_object   

          

if __name__=="__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.quickstart(Calculator(),'/',conf)
    #cherrypy.config.update({'server.socket_port':8090})