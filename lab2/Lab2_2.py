import cherrypy
import json

class Calculator(object):
    exposed=True
    def GET(self,*uri,**params):

        result=0
        uri=list(uri)

        for i in range(1, len(uri)):
            uri[i]=float(uri[i])

        if len(uri)!=0:
            if len(uri)==3:
                if uri[0] == 'add':
                    result = uri[1] + uri[2]
                elif uri[0] == 'sub':
                    result = uri[1] - uri[2]
                elif uri[0] == 'mul':
                    result = uri[1] * uri[2]
                elif uri[0] == 'div':
                    if uri[2] != 0:
                        result = uri[1] / uri[2]
                    else:
                        result = 'Division error'
                else:
                    result = 'Incorrect command'
            else:
                result = 'Invalid number of operands'
        
            dictionary={
                "operation": uri[0],
                "operands": {"op1": uri[1], "op2": uri[2]},
                "result": result,
            }
            json_object = json.dumps(dictionary, indent=3)
        return json_object   

          

if __name__=="__main__":
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.quickstart(Calculator(),'/',conf)
    #cherrypy.config.update({'server.socket_port':8090})