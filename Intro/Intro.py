if __name__=="__main__":
    #name = "Gabriella"
    #age = 23
    #date = "02/01/2020"
    #print(f"My name is {name} and I'm {age} years old. I was born the {date}")

    #pi = 3.14169265
    #print(f"PiGreco is {pi:.3}")

    #print(f"5+3={5+3}")

    #name = input("What's your name?")
    #print(f"Hi {name}!")

    #Ex5
    #f = open('Original.txt')
    #fileContent = f.read()
    #f.close()
    #f = open ('Copy.txt','w')
    #f.write(f'The content of the original file is: \n{fileContent}')

    #Ex6
    a = int(input("Write a number\n"))
    if a%2==0:
        if a%3==0:
            print(f'{a} is a multiple of 2 and 3')
        else:
            print(f'{a} is a multiple of 2 but not of 3')
    else:
        if a%3==0:
            print(f'{a} is a multiple of 3 but not of 3')
        else:
            print(f'{a} is neither a multiple of 2 or 3')
pass
