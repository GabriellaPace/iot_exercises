if __name__ == "__main__":
    #Ex7
    numbers=[1,1,2,2,1]

    # minimum=numbers[0]
    # maximum=numbers[0]
    # sum4avg=0
    # for item in numbers:
    #     if item<minimum:
    #         minimum=item
    #     if item>maximum:
    #         maximum=item
    #     sum4avg+=item
    
    maximum = max(numbers)
    minimum = min(numbers)
    sum4avg=0
    for item in numbers:
        sum4avg+=item

    print(f'In the list of numbers {numbers},\nthe min is {minimum},\nthe max is {maximum},\nthe average is {sum4avg%len(numbers)}.')
    pass