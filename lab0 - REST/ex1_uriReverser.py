import cherrypy

class UriReverser():
    exposed=True

    def __init__(self):
        pass
    def GET(self,*uri):
        reverseOut="empty"
        if len(uri)!=0:
            reverseOut=""
            for word in uri:
                reverseOut+=word[::-1]+'<br>'               
        return reverseOut

if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    #cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(UriReverser(),'/',conf)
