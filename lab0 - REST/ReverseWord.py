import cherrypy

class ReverseWord():
    exposed=True
    def GET(self,*uri,**params):
        output=""
        uri=list(uri)
        
        for i in range(len(uri)):
            uri[i] = uri[i][::-1]

        if len(uri)!=0:
            output+='Reversed uri: '+','.join(uri)

        if params!={}:
            output+='<params: '+str(params)
        return output
        
if __name__ == '__main__':
    conf={
        '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
            'tool.session.on':True
        }
    }
#cherrypy.config.update({'server.socket.port':8090})
cherrypy.quickstart(ReverseWord(),'/',conf)