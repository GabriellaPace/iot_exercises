import cherrypy

class HelloWorld(object):
    exposed=True
    def GET(self,*uri,**params):
        #Standard output
        output="Hello World"
        #Check the uri in the requests
        #<br> is just used to append the content in a new line 
        #(<br> is the \n for HTML)
        uri=list(uri)
        if len(uri)!=0:
            output+='<br>uri: '+','.join(uri)
        #Check the parameters in the request
        #<br> is just used to append the content in a new line 
        #(<br> is the \n for HTML)
        if params!={}:
            output+='<br>params: '+str(params)
        return output

if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    #cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(HelloWorld(),'/',conf)
    #http://127.0.0.1:8080/first/second/third?p1=a&p2=b
    #http://localhost:8080/first/second/third?p1=a&p2=b