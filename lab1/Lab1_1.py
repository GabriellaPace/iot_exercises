import json

error=0
command = input("Choose an operation:\n add: sum of two operands\n sub: subtraction of two operands\n mul: multiplication of two operands\n div: division of two operands, if possible\n exit: close program\n->")

if command != 'exit':
    a = int(input("Input the two operands:\n a= "))
    b = int(input(" b= "))

    if command == 'add':
        output= a+b
    elif command == 'sub':
        output= a-b
    elif command == 'mul':
        output= a*b
    elif command == 'div':
        if b != 0:
            output= a/b
        else:
            error = 1
            print(f"Can not compute the division by zero.")
            pass
        pass
elif command == 'exit':
    exit("ended")
    pass


if error == 0:
    dictionary={
        "operation": command,
        "operands": [{"a": a, "b": b}],
        "result": output,
    }

    print(json.dumps(dictionary, indent=3))
    pass