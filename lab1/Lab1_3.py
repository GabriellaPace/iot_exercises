import json
from datetime import datetime

tmp = open("catalog.json", "r")
data = json.load(tmp)
tmp.close()

command=''
while command != 'exit':
    flag=''
    command = input("Choose an operation:\n name: search by device name\n id: search by device id\n ser: search by provided services\n typ: search by measure type\n newd: insert a new device\n all: print the full catalog\n exit: save and close\n-> ")

    if command in{'name','id','ser','typ'}:
        req = input("Input the key for the research:\n ")
        
        if command == 'name':
            for device in data["devicesList"]:
                if device["deviceName"] == req:
                    print(json.dumps(device, indent=3))

        elif command == 'id':
            for device in data["devicesList"]:
                if device["deviceID"] == req:
                    print(json.dumps(device, indent=3))  

        elif command == 'ser':
            for device in data["devicesList"]:
                for service in device["availableServices"]:
                    if service == req:
                        print(json.dumps(device, indent=3))  

        elif command == 'typ':
            for device in data["devicesList"]:
                for typ in device["measureType"]:
                    if typ == req:
                        print(json.dumps(device, indent=3)) 

    elif command == 'newd':
        newid = input("Id: ")

        #check if it already exists:
        for device in data["devicesList"]:
                if device["deviceID"] == newid:
                    flag = 'update'

        if flag == 'update':
            print(f"Updating exixsting device with Id=", newid)
        else:
            print(f"Creating a new device with Id=", newid)

        #Creating a temporary dictionary with all the input
        tempDevice = {
            "deviceID":newid,
            "deviceName": input("Name: "),
            "measureType": input("Types (divided by comma): ").split(','),
            "availableServices": input("Services (divided by comma): ").split(','),  
        }
        tempDevice["servicesDetails"]=[]
        for i in range(len(tempDevice["availableServices"])):
            print(f"Insert information for  service: ", tempDevice["availableServices"][i])
            tempDevice["servicesDetails"].append({
                "serviceType": input("Service type: "),
                "serviceIP": input("Service IP: "),
                "topic": input("Service topics (divided by comma): ").split(',')
            })
        tempDevice["lastUpdate"]= datetime.now().strftime("%d-%b-%Y %H:%M")

        #print(json.dumps(tempDevice, indent=3)) 

        if flag == 'update':
            for device in data["devicesList"]:
                if device["deviceID"] == newid:
                    device = tempDevice           
        else:
            data["devicesList"].append(tempDevice)

    elif command == 'all':
        print(json.dumps(data, indent=3))

    elif command == 'exit':
        with open("catalog.json", "w") as outfile:
            json.dump(data, outfile, indent=3)
        outfile.close()
        print("Saved.")

    else:
        print("Invalid operation")

