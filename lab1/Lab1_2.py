import json

error=0
command = input("Choose an operation:\n add: sum of two operands\n sub: subtraction of two operands\n mul: multiplication of two operands\n div: division of two operands, if possible\n exit: close program\n-> ")

if command != 'exit':
    operands= input("Input all the operands separated by a comma:\n-> ")
    single_op= operands.split(',')
    for i in range(len(single_op)):
        single_op[i]=int(single_op[i])
        pass
    output= single_op[0]

    # print(single_op)

    if command == 'add':
        for i in range(1, len(single_op)):
            output += single_op[i]
    elif command == 'sub':
        for i in range(1, len(single_op)):
            output -= single_op[i]   
    elif command == 'mul':
        for i in range(1, len(single_op)):
            output *= single_op[i]      
    elif command == 'div':
        for i in range(1, len(single_op)):
            if single_op[i] != 0:
                output = output/single_op[i]         
            else:
                error = 1
                print(f"Can not compute the division by zero.")
                pass
        pass
elif command == 'exit':
    exit("ended")
    pass

if error == 0:
    dictionary={
        "operation": command,
        "operands": [operands],
        "result": output,
    }

    json_object = json.dumps(dictionary, indent=4)
    print(json.dumps(dictionary, indent=4))
    pass

# open("calculator.json", "w") as outfile: outfile.write(json_object)