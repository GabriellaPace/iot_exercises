import cherrypy
import json
class command():
    def __init__(self,x):
        self.x = x
       
    def add(self):
        res = sum(self.x)
        return res

    def sub(self):
        z = self.x
        res = 2*z[0] - sum(z)
        return res

    def mul(self):
        z = self.x
        res = 1
        for items in z:
            res = res*items
        return res

    def div(self):
        z = self.x
        res = z[0]
        for i in range(1,len(z)):
            res = res/z[i]
        return res

class ParamsLister():
    exposed = True

    def __init__(self):
        pass

    def PUT(self):
        body = cherrypy.request.body.read()
        jsonBody = json.loads(body)
        values = list(jsonBody.values())

        if values[0] == "add":
            output = (f"Operation: {values[0]} <br>Values: {values[1]} <br> Result: {command(values[1]).add()}")

        if values[0] == "sub":
            output = (f"Operation: {values[0]} <br>Values: {values[1]} <br> Result: {command(values[1]).sub()}")

        if values[0] == "mul":
            output = (f"Operation: {values[0]} <br>Values: {values[1]} <br> Result: {command(values[1]).mul()}")

        if values[0] == "div":
            output = (f"Operation: {values[0]} <br>Values: {values[1]} <br> Result: {command(values[1]).div()}")

        return str(output)
        #return f" Keys:{list(jsonBody.keys())}, values :{values[1][0]}"

if __name__=="__main__":

    #Standard configuration to serve the url "localhost:8080" 

    conf={
    '/':{
            'request.dispatch':cherrypy.dispatch.MethodDispatcher(), 
            'tool.session.on':True
        } 
}
cherrypy.quickstart(ParamsLister(),'/',conf) 