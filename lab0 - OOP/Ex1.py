import math

class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    #definition of methods
    def distance(self, second):
        return math.sqrt((self.x-second.x)**2+(self.y-second.y)**2)
    def move(self, X, Y):
        self.x+= X
        self.y+= Y
    #method used to print the content of a and not its address
    def __repr__(self):
        return(f"({self.x}, {self.y})")


if __name__=="__main__":    
    a=Point(7,1)    
    b=Point(1,1)    
    print(f"The distance between the points is: d = {a.distance(b)}")  

    a.move(2,2)    
    print(f"The new position of a is: {a}")