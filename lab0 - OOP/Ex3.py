from Ex1 import *
if __name__ == "__main__":
    while True:
        command = input("d: for distance among two pints, \n m: to move a point.\n q: to quit.\n")
        if command == "d":
            xa = float(input("xa: "))
            ya = float(input("ya: "))
            xb = float(input("xb: "))
            yb = float(input("yb: "))
            a = Point(xa, ya)
            b = Point(xb, yb)
            print(f"The distance between the points is d={a.distance(b)}")
        if command == "m":
            xa = float(input("xa: "))
            ya = float(input("ya: "))
            x2 = float(input("x2: "))
            y2 = float(input("y2: "))
            a = Point(xa, ya)
            print(f"The distance between the points is d={a.move(x2, y2)}")
        if command == "q":
            break