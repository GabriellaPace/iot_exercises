import json

class ContactManager():
    def __init__(self, fileName):
        self.fileName = fileName
        self.contactsList = []
    def loadContacts(self):
        fp = open(self.fileName)
        td = json.load(fp)        #temporary dictionary
        for contact in td["contacts"]:
            newContact = {
                "name":contact["name"], 
                "surname":contact["surname"], 
                "mail":contact["mail"]}
            self.contactsList.append(newContact)
        
    def showContacts(self):
        for contact in self.contactsList:
            print(contact)

if __name__ == "__main__":
    cm = ContactManager("OOP/contacts.json") #otherwise "../" = go outside of the folder, NameFolder/etc
    cm.loadContacts()
    cm.showContacts()
    #Ex5:  json.dump(contactsList, open("OOP/contacts.json", 'w'))
