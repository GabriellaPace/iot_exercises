import math

class SquareManager():
    #definition of the class
    def __init__(self, side): #I create you with a "side" chacteristic
        self.side = side     #when I create you, the number that I give you is the side
    
    #definition of methods
    def area(self):
        return self.side**2
    def perimeter(self):
        return self.side*4
    def diagonal(self):
        return math.sqrt(2)*self.side

# if __name__ == "__main__":
#     sm=SquareManager(3)    
#     print(f"The area of the square with side {sm.side} = {sm.area()}")    
#     print(f"The perimeter of the square with side {sm.side} = {sm.perimeter()}")    
#     print(f"The diagonal of the square with side {sm.side} = {sm.diagonal():.3f}")

#     pass